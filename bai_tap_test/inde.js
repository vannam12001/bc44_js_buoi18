var numArr = [];
// lấy number
document.getElementById("inKetQua").onclick = function () {
  var numberEL = document.querySelector("#number");
  var number = numberEL.value * 1;
  var minSoDuong = numArr[0];
  numArr.push(number);
  document.getElementById(
    "result"
  ).innerHTML = `<h5>Các số bạn đã nhập : ${numArr}</h5>`;
  numberEL.value = "";
};
// Tính tổng các số dương
document.getElementById("sumDuong").onclick = function () {
  var sumSoDuong = 0;
  for (var index = 0; index < numArr.length; index++) {
    if (numArr[index] > 0) {
      sumSoDuong += numArr[index];
    }
  }
  document.getElementById(
    "result1"
  ).innerHTML = `<h5>Tổng số dương: ${sumSoDuong}</h5>`;
};
// Đếm số dương
document.getElementById("demDuong").onclick = function () {
  var soLuongSODuong = 0;
  for (var index = 0; index < numArr.length; index++) {
    if (numArr[index] > 0) {
      soLuongSODuong++;
    }
  }
  document.getElementById(
    "result2"
  ).innerHTML = `<h5>Số lượng số dương: ${soLuongSODuong}</h5>`;
};
// Tìm số nhỏ nhất
document.getElementById("soNhoNhat").onclick = function () {
  var min = numArr[0];
  for (var index = 0; index < numArr.length; index++) {
    if (numArr[index] < min) {
      min = numArr[index];
    }
  }
  document.getElementById("result3").innerHTML = `<h5>Số nhỏ nhất: ${min}</h5>`;
};
// Tìm số dương nhỏ  nhất
document.getElementById("soDuongNhoNhat").onclick = function () {
  // tạo mảng chứa số dương
  var numArrDuong = [];
  for (var index = 0; index < numArr.length; index++) {
    if (numArr[index] > 0) {
      numArrDuong.push(numArr[index]);
    }
  }
  // tìm min số dương
  if (numArrDuong.length > 0) {
    for (var i = 0; i < numArrDuong.length; i++) {
      var minDuong = numArrDuong[0];
      if (numArrDuong[i] < minDuong) {
        minDuong = numArrDuong[i];
      }
    }
    document.getElementById(
      "result4"
    ).innerHTML = `<h5>Số dương nhỏ nhất: ${minDuong}</h5>`;
  } else {
    document.getElementById(
      "result4"
    ).innerHTML = `<h5>không có số dương trong mảng </h5>`;
  }
};
// Tìm số chẵn cuối cùng
document.getElementById("chanCuoi").onclick = function () {
  var chanCuoi = 0;
  for (var index = 0; index < numArr.length; index++) {
    if (numArr[index] % 2 == 0) {
      chanCuoi = numArr[index];
      document.getElementById(
        "result5"
      ).innerHTML = `<h5>Số chẵn cuối cùng : ${chanCuoi} </h5>`;
    } else {
      document.getElementById("result5").innerHTML = -1;
    }
  }
};
// Đổi chỗ
document.getElementById("doiCho").onclick = function () {
  var index1 = document.getElementById("index1").value * 1;
  var index2 = document.getElementById("index2").value * 1;
  function swap(index1, index2) {
    var iSo = numArr[index1];
    numArr[index1] = numArr[index2];
    numArr[index2] = iSo;
  }
  swap(index1, index2);
  document.getElementById("result6").innerHTML = `mảng sau khi đổi ${numArr}`;
};

// Sắp xếp tăng dần
document.getElementById("sapXep").onclick = function () {
  numArr.sort(function (a, b) {
    return a - b;
  });
  document.getElementById(
    "result7"
  ).innerHTML = `mảng sau khi sắp xếp ${numArr}`;
};

// tìm số nguyên tố
function timSoNgTo(n) {
  var flag = true;
  if (n < 2) {
    flag = false;
  }
  for (var i = 2; i <= Math.sqrt(n); i++) {
    if (n % i == 0) {
      flag = false;
    }
  }
  return flag;
}

document.getElementById("soNgTo").onclick = function () {
  for (var i = 0; i < numArr.length; i++) {
    var soNgTo = numArr[0];
    if (timSoNgTo(numArr[i]) == true) {
      soNgTo = numArr[i];
      break;
    }
    document.getElementById("result8").innerHTML = `${soNgTo}`;
  }
};
